\label{sec:cases}

Taint tracking results can be used to help protect sensitive data and prevent information
leaking on GPUs. We describe two major use cases of taint tracking analysis and
present our prototype implementation of the tainting-enabled data protection. 

%The second use to monitor the data that is sent
%between GPUs, CPUs and network devices, establish the relationship between the
%tainted GPU data and tainted CPU data, completing the whole CPU-GPU taint
%tracking system. 

\subsection{Sensitive Data Removal}

Lee et al.~\cite{lee-sp-2014} and Pietro et al.~\cite{pietro-tecs-2016}
have recently demonstrated that information leaking from one program to another may occur in
GPU local memory between GPU kernel executions, and in GPU global memory between program runs.
Our taint tracking results may help a program understand the propagation of certain sensitive
information and clear all taints before relevant points of vulnerability (e.g., clearing local
memory taints at the end of each kernel and clearing global memory taints at the end of
program run). 

We make a prototype implementation of this use case. For registers, we let every
thread clear its own tainted registers. It is possible that some threads exit
earlier than others. However since register taint map is thread-private, we can
insert the clearing code right before every \emph{EXIT} point and thus
early-exiting threads can also clear their tainted registers early. For local
memory, since it is thread-private, we treat it the same way as registers. Note that registers and
local memory cannot be cleared by programmers themselves (unlike shared memory
and global memory) and thus a trustworthy binary instrumentation tool is
necessary to prevent sensitive data from leaving taints on GPUs.

For shared memory, since shared memory is visible to all threads in the same basic
block, we need to make sure the sensitive shared memory data is cleared
\emph{after} all threads in the same thread block finish their work. Therefore,
our design is to create a control flow reconvergence point for all threads since different threads might take different
execution paths. We then insert a thread block level barrier at the reconvergence
point before clearing the tainted shared memory data. 

Pietro et al.~\cite{pietro-tecs-2016} proposed a register-spilling based attack,
which makes use of compiler to force spilling the registers so that the encryption key
(or reversibly transformed encryption key) in the AES encryption module in the program \emph{SSLShader} can be
moved from registers to local memory. Then a second running application can
steal the leaked information in local memory. Our taint clearing approach
successfully prevents this type of attacks by clearing the registers, local
memory, and shared memory right before every thread in the GPU application completes. 

Experimental results in Section~\ref{sec:eval} will show that the data clearing
cost is low.  Our proposed strategy incurs a slowdown of no more than 13.05\%
compared with the original program execution time, and in most cases the
slowdown is no more than 5.1\%.

\subsection{GPU Malware Countermeasure} 

With GPU taint analysis, we learn where and
when the sensitive data is sent from GPU device to CPU and/or other network
devices. This is especially important for integrated CPU-GPU whole system taint
tracking. A dynamic taint tracking system that only monitors data dependences
during CPU execution may miss the influence propagation of untrusted inputs or
execution results through GPU computation. For example, GPU malware
Keylogger~\cite{ladakis-eurosec-2013} and Jellyfish~\cite{jellyfish-web}
exploited direct memory access (DMA) at mapped CPU memory to snoop the CPU
system activities and steal host information.
GPU may obtain the leaked CPU information, process it, and send it through
a network or other output device while presenting no opportunity for any countermeasure
that only monitors CPU executions.

Our GPU data protection system can not only clear sensitive data, but can also capture possible
attempts of stealing and emitting sensitive information.  We prevent
this type of attacks by dynamically monitoring the data transfer between CPU
and GPU\@.  If the GPU-mapped CPU memory contains sensitive information
(i.e., keystroke buffer in the Keylogger attack~\cite{ladakis-eurosec-2013}),
the mapped data region is marked as taint sources.  We track the dependency
propagation of tainted data in GPU executions.
Further we statically instrument memory transfer APIs so that before any
data is sent from GPU through cudaMemcpy APIs in CUDA or
clEnqueueReadBuffer APIs in OpenCL, the range of the memory address is checked. If the transmitted data
falls within the sensitive tainted memory range, we either alert the system
that tainted data is transmitted, or mark the corresponding CPU destination memory region (if data is
transferred back to CPU) as tainted. Since all communication between GPU
and other devices rely on explicit memory transfer API, we can check and protect
information flow by instrumenting these memory transfer APIs. 

Our taint tracking and data protection system helps protect applications that
utilize both CPU and GPU, if combined with CPU taint tracking. Our work ensures
full system taint tracking that is essential to whole system security. We are
not aware of any other work that provides the same degree of protection. Besides the Keylogger
 case, other potential whole-system tracking examples include a web site that relies on taint tracking to prevent untrusted user inputs from forming malicious
database queries (e.g., through SQL injections) or invoking dangerous system calls
(e.g., through buffer overflow attacks).  

Finally, when GPU tainting is securely applied to untrusted programs, it can
also identify malicious programs that attempt to scan uninitialized data which
may have been left by previous kernel and
program runs from other users. We haven't implemented this type of data
protection.  However, our tool can be readily extended to help detect
uninitialized memory region containing sensitive data left by prior GPU
program execution and clear\,/\,zero them if appropriate.

%If a second application reads the memory that happens
%to be allocated by the last running application, before the memory is
%initialized, the second running application can potentially be a malicious
%application that aims to steal information from other GPU programs or users. 

%Sometimes it is desirable to protect sensitive data before the end of its lifetime.  Much like
%CleanOS~\cite{tang-osdi-2012} in smartphone data protection, tainting can help protect live
%but inactive sensitive data through encryption.  The objective in this case is to reduce
%(but not to eliminate) the exposure of sensitive data.
%This protection is useful on mobile platforms where the device may physically fall into the possession
%of a malicious party (through loss or theft) and its entire
%memory may be inspected (e.g., through cold boot RAM imaging attacks~\cite{halderman-uss-2008}).
%This protection is also useful in cases when concurrently running GPU programs may see each other's
%global memory.  \notered{Could this only happen in the ``register spilling'' case described
%in~\cite{pietro-tecs-2016}?  I am not sure this can be argued as a general threat.}


