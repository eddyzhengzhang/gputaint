\label{sec:back}

GPU functions, also known as kernel functions, make use of memory which is not
directly accessible from the CPU. GPU memory is split into several regions,
both on-chip and off-chip. On-chip memory consists of registers, caches, and
scratch-pad memory (called \emph{shared memory} in NVIDIA terminology). Note
that we use NVIDIA terminology throughout this paper. Off-chip memory is GDDR
SGRAM, which is logically distributed into texture
memory, constant memory, local memory, and global memory, with texture memory
and constant memory mapped to dedicated texture cache and constant caches.  %~\cite{TODO_Cuda_C_Programming_Guide}

Both texture memory and constant memory are read-only during the GPU kernel
execution. Therefore, in this
paper we focus on registers, shared memory, local memory, and global
memory. Shared memory is available to the programmer, often treated as a
software cache. Local memory is thread-private, and is most commonly used for
register spilling. Global memory is visible to the entire GPU device, and is
typically used as input and output for GPU functions. In all four of these
memory types, data persists after deallocation~\cite{pietro-tecs-2016}.
%Global memory is explicitly allocated and deallocated by the programmer through API functions, but the other three memory types are repeatedly allocated and deallocated by the GPU for each thread block as it launches and exits.

Global memory can be set and cleared through API functions, with overhead
similar to that of running a GPU kernel, but local memory, shared memory, and
registers are only accessible from within a kernel function, and allocated and
deallocated by the driver. These three memory types can only be
reliably cleared through instrumentation. Moreover, local memory and registers
are managed by compilers and they can only cleared by compile-time instrumentation.

%For the CPU, taint analysis may be performed system-wide, such as through the use of a virtual machine~\cite{TODO}. Since the drivers and hardware are closed source, it is infeasible to perform system-wide taint analysis on the GPU. Instead, we perform taint analysis per-process through binary-level instrumentation.
%Since numerous threads execute simultaneously, there is a possibility for data races in the taint map, but if the original program is deterministic, then we can guarantee that the taint analysis is also deterministic.

Sensitive information can also
propagate to different data storage locations on GPU: memory, software caches, and
registers. An example is the advanced encryption standard (AES), in which the
key and the plain text to be encrypted may reside in different types
of memory~\cite{pietro-tecs-2016}. They can be stored in global memory as allocated data objects and in registers as program execution operands. We show
later in this paper that key theft or plain text theft can happen by
stealing information from registers (Section \ref{sec:eval}).
% \notered{?? to add or not, depending on Ari's experiments}. 

There is less memory protection on GPUs as compared with CPUs. When two
applications run simultaneously on the same GPU with the Multi-process Service (MPS),
one application can peek into the memory of another application, documented in
NVIDIA's MPS manual at Section 2.3.3.1, ``\emph{An out-of-range read in a CUDA
Kernel can access CUDA-accessible memory modified by another process, and will
not trigger an error, leading to undefined behavior}." 
When two applications do
not run simultaneously, in which case every application will get a serially scheduled
time-slice on the whole GPU, there is still a good chance to leak information. The second running application can read data left by the first
running application if  it happens to allocate the same memory locations as the
first one. This vulnerability has been confirmed and stated by a number of
previous works~\cite{vasiliadis-ccs-2014,pietro-tecs-2016,lee-sp-2014}. 
%\notered{Does \cite{vasiliadis-ccs-2014} really confirm this?}
