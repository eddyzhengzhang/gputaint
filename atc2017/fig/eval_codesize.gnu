set terminal pdf size 5,2.5        # output format
set output 'eval_codesize.pdf' # output file

set style data histogram # graph style
set style histogram cluster gap 1 # cluster histogram
#set size ratio 0.2
#set key left
set key above

set style fill solid 1 border rgb "black"
set xtics rotate by -40
#set xtics border offset -2.75,-2.0,0
#set xtics font "Times-Roman, 10"
set auto x # arbitrary x
set auto y # arbitrary y
set ylabel "Normalized code size"
set yrange [0:12] 
set ytics 3

plot 'eval_codesize.dat' using 2:xticlabels(1) title columnheader(2) linecolor rgb "black" lw 1.5 fs pattern 7, \
                      '' using 3:xticlabels(1) title columnheader(3) linecolor rgb "gray" lw 1.5, \
                      '' using 4:xticlabels(1) title columnheader(4) linecolor rgb "black" lw 1.5 fs pattern 2, \
                      '' using 5:xticlabels(1) title columnheader(5) linecolor rgb "white" lw 1.5, \
                      '' using 6:xticlabels(1) title columnheader(6) linecolor rgb "black" lw 1.5 fs pattern 6, \
                      '' using 7:xticlabels(1) title columnheader(7) linecolor rgb "black" lw 1.5
