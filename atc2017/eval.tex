\label{sec:eval}

We perform evaluation on a machine configured with an NVIDIA GTX 745. This is a
``Maxwell" generation GPU with compute capability 5.0. Since NVIDIA's compiler and binary ISA 
are closed-source, we modify the GPU binaries using tools inspired by the asfermi~\cite{hou2011asfermi} 
and MaxAs~\cite{scott2014maxas} projects, allowing for binary instructions be directly inserted into 
the executable.

\vspace{-0.1in}
\subsection{Benchmarks}
\vspace{-0.1in}

Our evaluation employs a variety of GPU kernels in deep learning, image
processing, and data encryption.  First, Caffe~\cite{jia2014caffe} is a deep learning framework
in which a user writes a ``prototxt'' file describing the input and layers of
the deep learning network (e.g., convolutional layers, inner-product layers, etc.),
which can be fed into the Caffe executable to create, train, and test the network.
Newer versions of Caffe allow various layers to be executed on the GPU via CUDA\@.
A common use of Caffe is image classification.  We use three Caffe kernels in our
evaluation: im2col, ReLUForward, and MaxPoolForward. These three kernels consume
the majority of the execution time for image classification.

We additionally use kernel functions from the CUDA SDK~\cite{CUDASDK}, the Rodinia 
benchmark suite~\cite{che2009rodinia}, and SSLShader~\cite{jang2011sslshader}. 
From the CUDA SDK we include BlackScholes, a program for financial analysis, and FDTD3d, 
a 3D Finite Difference Time Domain solver. As a numerical analysis program, FDTD3d is unlikely 
to have sensitive data to protect, but serves as an additional data point for testing our performance.
From Rodinia, we include Needleman-Wunsch, a bioinformatics benchmark used for DNA sequencing.
From SSLShader, we include an AES encryption program.

\vspace{-0.1in}
\subsection{Taint Analysis \& Optimizations}
\vspace{-0.1in}

We evaluate the effectiveness of the two performance enhancing techniques in Section~\ref{sec:tainting}---%
taint reachability filtering and taint map in registers.

% \begin{table*}[t]
% \begin{center}
% \begin{tabular}{|l||r|r|r|r|r|r|}\hline
% \emph{GPU kernels} & \emph{Naive} & \emph{Taint Map in Registers} & \emph{Forward Filter} & \emph{Backward Filter} & \emph{Two Pass Filter} & \emph{Fully Optimized} \\
% \hline
% im2col & 21.70 & 5.32 & 2.34 & 14.21 & 2.34 & 2.15 \\
% ReLUForward & 10.78 & 2.65 & 2.61 & 9.26 & 2.36 & 1.80 \\
% MaxPoolForward & 26.43 & 6.39 & 4.34 & 20.60 & 4.32 & 1.97 \\
% FDTD3d & 30.19 & 6.94 & 22.76 & 30.04 & 22.77 & 5.73 \\
% BlackScholes & 19.35 & 4.71 & 16.91 & 17.78 & 15.73 & 3.43 \\
% SSLShader & 51.77 & 7.34 & 33.85 & 15.45 & 10.49 & 2.52 \\
% needle & 27.59 & 4.68 & 13.41 & 25.68 & 13.34 & 3.56 \\
% \hline
% \end{tabular}
% \caption{GPU tainting slowdown ratio normalized to native GPU kernel execution.  \notered{Should change to a bar graph.}}
% \label{tab:slowdown}
% \end{center}
% \end{table*}

Since we modify the executable directly, we measure the cost of taint analysis in terms of both 
slowdown and code size. There are a few factors which exacerbate these costs. Whenever 
we insert an instruction to get or set a location's taintedness in memory, we first have to 
calculate its address. Since addresses for global memory are 64-bit on this architecture, but 
registers and integer operations are 32-bit, this requires multiple instructions with immediate 
dependencies.

Additionally, each thread has access to only one carry flag, so if it is already in 
use where we need to get the taint address, extra instructions are needed to spill it into a register or to memory. Furthermore, the singular carry flag makes 
it difficult to interleave instructions effectively, since they may overwrite each other's result.
As the GPU is incapable of out-of-order execution within a thread, the latency for accessing 
the taint-map is costly.

\begin{figure}[htp]
\begin{subfigure}{\columnwidth}
\centering
\includegraphics[width=\linewidth]{fig/eval_slowdown.pdf}
\vspace*{-0.25in}
\caption{Normalized slowdown from instrumentation.}
\label{fig:eval_overhead_a}
\end{subfigure}
\begin{subfigure}{\columnwidth}
\centering
\includegraphics[width=\linewidth]{fig/eval_codesize.pdf}
\vspace*{-0.25in}
\caption{Normalized code size after instrumentation.}
\end{subfigure}
\caption{Overhead of tainting instrumentation.}
\vspace*{-0.1in}
\label{fig:eval_overhead}
\end{figure}

Figure~\ref{fig:eval_overhead}(a) illustrates the GPU tainting slowdown with each of our optimizations, 
compared to native execution. The `naive' bar shows slowdown without any optimizations, the `reg-in-reg' 
bar shows the results of placing part of the taint map into registers, the `forward-filter' and `backward-filter' 
bars show the results of each filter pass, the 
`two-pass-filter' bar shows results when using both filter passes, and
the `fully optimized' bar shows results when using all of these optimizations.
Figure~\ref{fig:eval_overhead}(b) shows normalized code sizes
(static instruction counts) for the same cases.

Figure~\ref{fig:eval_overhead} shows that both
two-pass filtering and hot register taint map can reduce the tainting cost
significantly.  For the filter passes,
there is a high correlation between relative slowdown 
and code size after instrumentation.
Saving taint mapping into registers does not shrink as much of the code size as
two-pass filtering, but
it still improves the tracking performance significantly. The tracking cost
saving comes more from the reduced memory latency than from reduced instruction count.

\vspace{-0.1in}
\paragraph{Taint Map in Registers}
Even on its own, saving part of the taint map in registers reduces significant time during taint analysis. 
The main alternatives, local memory and global memory, are both off-chip memories that may take 
hundreds of cycles to access. Even the cache to which such memory is saved is off-chip, because 
the on-chip L1-cache is typically only used for read-only data on newer architectures~\cite{maxwelltuningguide}. 
Since most GPU programs have numerous threads running at once, some of this latency is hidden by 
some threads continuing to execute while others wait for memory accesses to complete, but even so, 
saving register taint information into registers reduces slowdown compared to naive taint tracking in 
our benchmarks by 78\% on average.

\vspace{-0.1in}
\paragraph{Filtering}
The forward pass filtering also saves significant time, though it has more variance across different 
benchmarks. Its effectiveness stems from the properties of GPU kernels. Most kernels make use 
of non-taintable, read-only data such as thread ID and grid size to perform many calculations. Additionally, 
function parameters are read-only in GPU functions, making it impossible for them to become tainted in 
most programs. On its own, the forward pass reduces slowdown in our benchmarks by an average of 53\%.

\begin{table}[thb]
\begin{center}
\begin{small}
\begin{tabular}{|l||c|c|c|c|}
\hline
kernel & para- & imme- & const  & thread \\
       & meter & diate & mem.   & block id \\
\hline
im2col & 85\% & 85\% & 29\% & 64\% \\
ReLUForward & 20\% & 40\% & 47\% & 57\% \\
MaxPoolForward & 70\% & 72\% & 57\% & 58\% \\
FDTD3d & 17\% & 17\% & 11\% & 12\% \\
%FDTD3d & \% & \% & \% & \% \\
\hline
\end{tabular}
\end{small}
\caption{Percentage of filtered-out instructions for various reasons.}
\vspace*{-0.3in}
\label{tab:filterdetail}
\end{center}
\end{table}

We also analyze the reason why we are able to filter out a significant number of
instructions for some applications in the forward pass.
Table \ref{tab:filterdetail} shows the percentage of filtered out instructions
under different categories.
\emph{Parameter} means one or more source registers are from the (constant-memory) kernel parameters.
\emph{Immediate} means one or more source operands are immediate numbers.
\emph{Const memory} means at least one source is from constant memory.
Finally, \emph{thread\,/\,block id} means the influence is from the identifier of the current
thread or thread block.
The identifiers are stored in special registers private to each thread or
constant memory depending on the architecture, but in either case they
are known at static-time.
While it might be surprising that the sum of percentages due to multiple reasons may exceed 100\%,
note that an instruction may be filtered out due to multiple reasons.

We discover that most instructions are filtered out because of these four
categories.
The reason is that GPU programs distribute workload among threads based on their ids.
To get the assigned workload, each thread must perform a lot of computation using ids, immediate, and constant memory values (e.g., thread block \& grid dimensions).
The computation results, together with parameters (e.g., the start address of an array), are used to fetch assigned data.
Then the real computation starts as well as the taint tracking.
For most GPU programs, the real computation is short with several instructions, and the preprocessing including address calculation consumes most of the time.
That is why we can filter out most instructions in our forward pass: most
instructions do preparation work and are not related to the potentially tainted input data.
For FDTD3d, the computation is more complex and fewer instructions are filtered out.
It also explains why FDTD3d does not benefit from two-pass filtering as much as
compared with other benchmarks, as shown in Figure \ref{fig:eval_overhead}(a).

The backward pass is usually less effective than the other optimizations. While a lot of the inputs to 
a kernel function are effectively constants, the only means of returning anything is through global memory.
As such, we can expect that most operations will produce values which influence memory. Regardless, the 
backward pass does provide some benefit in most cases, and in the SSLShader benchmark it reduces 
slowdown compared to the naive approach by 22\%.

\vspace{-0.1in}
\paragraph{Combined Optimizations}
Compared to the forward pass, the two-pass filter reduces slowdown by 12\% on average, and compared 
to the backward pass, it reduces slowdown by 50\%.
Full optimization reduces slowdown by 56\% compared to the two-pass filter, and 42\% compared to only 
keeping part of the taint map in registers.
This demonstrates the merit of combining our different optimization techniques, which together reduce 
slowdown by an average of 87\%.

With full optimizations, our benchmarks' kernel functions experience an average normalized runtime of
3.0$\times$
after instrumentation. The FDTD3d benchmark suffers the worst slowdown at 5.7$\times$ runtime,
due to frequent use of shared memory making the filter less effective. The Needleman-Wunsch 
benchmark, which also has shared memory usage, is the next slowest with a 3.6$\times$ runtime. Although the
SSLShader benchmark also makes use of shared memory, it only uses shared memory to store
compile-time constants for faster retrieval, 
allowing us to filter out all shared memory instructions for less runtime slowdown of 2.5$\times$.

One special consideration when modifying GPU programs is occupancy---the number of threads that can be 
live at once. A high occupancy means that latency is less costly, as the GPU can switch to different groups 
of threads every cycle. Since our instrumentation results in additional use of registers, and the register file 
is evenly split among all live threads, there is potential for occupancy to be decreased, hurting performance 
more drastically. In such a case, it may be more beneficial not to store any part of the taint map into registers.
However, in practice, we use few enough additional registers that reducing occupancy is unlikely, since for 
every 32 registers in the original program, we only need 1 extra register to store their taintedness. We find 
that GPU programs typically use less than 64 registers per-thread, and so none of our benchmarks require more than 
two extra registers per-thread for storing register taintedness.

% \notered{For the memory reachability filtering, we'd also like to know how much of the instructions we filtered.}

\vspace{-0.1in}
\subsection{Memory Protection}
\vspace{-0.1in}

We next evaluate the incorporation of memory protection into our dynamic analysis framework. As discussed in 
Section~\ref{sec:cases}, the GPU does not clear memory before deallocation. This includes all types of memory, 
both on-chip and off-chip.
\cite{pietro-tecs-2016} demonstrates that data left behind even in local memory and shared memory can be stolen, 
such as the encryption key and plaintext in the SSLShader benchmark. We have found that this data can also
be stolen directly from registers by preparing a kernel function with the same thread block size and occupancy as the 
victim kernel function---thereby ensuring the register file will be partitioned in the same, predictable manner---%
and then manually coding the eavesdropping kernel's binary to read the desired registers.

Programmers can manually erase global memory before program exit, but registers and local memory 
are allocated by the compiler and cannot be as easily cleared. Sensitive data in registers, 
local memory, and also shared memory must be cleared before the kernel function exits, or else a malicious 
kernel function may be invoked and acquire these resources for itself. We leverage our instrumentation 
framework to clear sensitive data in these regions, via additional modification to the binary code. This can be 
used to prevent attacks such as the one in \cite{pietro-tecs-2016}, which stole encryption key data through 
such resources. The results are summarized in Table~\ref{tab:eval_erase}.

\begin{table}[t]
\begin{center}
\begin{tabular}{|l||r|r|}\hline
\emph{GPU kernel} & \emph{Memory} & \emph{Slowdown} \\
\hline
im2col & N/A & 0.26\%\\
ReLUForward & N/A & 0.33\%\\
MaxPoolForward & N/A & 0.59\%\\
FDTD3d & Shared & 5.10\%\\
BlackScholes & N/A & 0.40\%\\
SSLShader & Local & 0.41\%\\
needle & Shared & 13.05\%\\
\hline
\end{tabular}
\caption{Slowdown from memory erasure during kernel execution, measured as a
fraction of the original kernel time. ''Memory" column indicates which memory
types need to be cleared (besides registers).}
\vspace{-0.1in}
\label{tab:eval_erase}
\end{center}
\end{table}

Since registers and local memory are thread-private, they can be safely cleared by each thread prior to 
exit. We insert instructions to clear this data before the EXIT instruction, using the results of our 
forward-filter pass to avoid unnecessary work. But shared memory is shared by every thread in a thread 
block, and therefore may not be safe to erase until all of its threads finish execution. Before 
the EXIT instruction we insert a synchronization barrier, which causes threads to halt until all other 
threads in the block reach the same point, and then add a loop which has every thread zero out a 
separate portion of shared memory. In benchmarks with less regular control flow, where threads 
exit at different points in the code, we can instead have shared memory cleared by a subset of its threads.

We find that the cost to clear tainted registers is trivial, adding only a fraction of a percent to runtime.
Each register takes only one cycle of amortized time to erase for every 32 threads, and the GPU is likely 
able to overlap most of these cycles with memory stalls from other threads.
None of our benchmarks use local memory by default, since it is usually used for register spilling. In order 
to evaluate the slowdown of clearing local memory, we recompile SSLShader, which uses 
40 registers, to instead use 20 registers. Clearing local memory and registers in this benchmark adds 
0.41\% time overhead.

Shared memory is slower to clear. In FDTD3d, clearing taints in shared memory adds 5.10\% runtime 
compared to the original kernel function, and in Needleman-Wunsch it adds 13.05\%. The increased 
slowdown compared to clearing local memory likely stems from the use of a loop, due to the GPU's 
inability to perform speculative and out-of-order execution, forcing a thread to wait until each shared 
memory location is cleared until it can zero the next one. Local memory is simpler to handle, with 
every thread accessing the same logical addresses despite using different physical locations, 
allowing for the local memory clearing loop to be fully unrolled.

Using the taint information to erase only sensitive data can help significantly, compared to naively 
clearing these memories fully. For example, in the SSLShader benchmark the tainted registers and 
local memory are cleared in 47\,mSecs, but this benchmark makes use of shared memory which is never 
tainted. If its shared memory arrays are erased, in addition to clearing the small amount of registers 
and local memory in their entirety, then the overhead would jump to 407\,mSecs.

% \notered{We should also evaluate some data protection case studies in Section~\ref{sec:cases}.
% For the key protection of SSLShader, we should probably evaluate the cost of clearing 
% tainted data.  Can we also somewhat validate that our system indeed captures sensitive data
% that would otherwise be leaked?
% We should probably also evaluate the effectiveness and cost of the Jellyfish case study.
% }
