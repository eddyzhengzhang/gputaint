GPUs have been widely used in many important application domains
beyond scientific computing, including
machine learning, graph processing, data encryption,
computer vision, etc. Sensitive information propagates into
GPUs and, while being processed, leaves traces in GPU memory. For example, in
a face recognition application, besides the input photo itself, the features
extracted at different levels of the deep learning neural networks may also contain
part of sensitive or private information. 
Figure~\ref{fig:dnnexample} shows the
extracted features from the first level of neural networks in a face recognition
program, where Figure~\ref{fig:dnnexample}(a) is the original picture and 
Figure~\ref{fig:dnnexample}(b) are features such as silhouette of a human face. 
Given a sensitive input user photo,
features in deep learning applications
may contain much of the sensitive data as well. Other sensitive data
in today's GPU applications include encryption keys, digits in personal checks,
license plates, location information in virtual reality apps, etc. If not
tracked or protected, sensitive information can be inadvertently leaked or stolen by
malicious applications on GPUs.

\begin{figure}[htp]
\centering
\begin{subfigure}{0.11\textwidth}
\centering
\includegraphics[width=1.0\linewidth]{fig/dnnexample_org.pdf}
\caption{Org. Photo}
\end{subfigure}
\begin{subfigure}{0.33\textwidth}
\centering
\includegraphics[width=1.0\linewidth]{fig/facerecog.png}
\caption{Extracted Features}
\end{subfigure}
\caption{Neural network information leaking example.}
\label{fig:dnnexample}
\end{figure}

%A paper has shown that key theft can happen on GPUs
%through xxx (cite the theft paper) Ari mentioned. 

%While today essentially all deep
%learning programs are running on GPUs thanks to its massive parallelism, people's
%private information such as , or recognition of personal checks, car license plate, street locations, their private
%information such as their phones, locations, finance information can potentially
%be leaked. 

Taint analysis~\cite{chow-uss-2004,newsome-ndss-2005,ho-eurosys-2006,qin-micro-2006,xu-uss-2006,clause-dytan-2007,zhu-osr-2011,Enck+:TOCS2014}
is a powerful tool for information flow tracking and security.
It tracks where and how sensitive information flows during program execution. Taint
analysis is a form of data flow analysis, wherein an input set of sensitive data is marked as ``tainted", and this taint
is tracked during runtime as it spreads into different locations in memory via
move, arithmetic, and control operations. Taint analysis results can be used to protect data by
clearing tainted variables at the end of its life range---for instance,
the temporary key schedule at every round of AES algorithm---or by encrypting
live but inactive tainted data~\cite{tang-osdi-2012}.
Taint analysis can also help identify and counter abnormal behaviors of malicious malware.
Existing dynamic taint analysis has primarily been applied to CPU programs though
its functions are increasingly desirable for GPUs as well.

%Overall, taint analysis is an important
%component for software level data protection on GPUs,   (can shorten, how taint analysis can be
%used to reduce information leak or security threats)

This paper presents the first design and implementation of a GPU taint
tracking system.  Our approach is based on static binary instrumentation that
enables dynamic taint tracking of a GPU program.  In comparison to dynamic instrumentation that
captures and modifies instructions on the fly, our approach does not require a dynamic instrumentation framework
or virtual machine emulation that is not readily available on GPUs.  We perform static instrumentation
on GPU program binaries without source access so that it is easy to apply in
practice.
We instrument programs on a
per-application basis and when the program runs, every thread can
dynamically track information flow by itself.

%However, it is infeasible to do
%the same on GPUs since there are thousands of threads running
%simultaneously, making dynamically instrumenting every instruction
%from every thread prohibitively expensive. 
%\notered{There are more threads of work, but also more hardware threads that can
%do the work.  Is the above a good argument?}

The major challenge for efficient taint tracking is that tracking
every dynamic binary instruction is expensive.  Our solution exploits the
fact that a large portion of a typical GPU program execution operates on un-taintable
runtime parameters and constants.  Examples include the logical thread indexes, 
thread block identifiers, dimension configurations, and pointer-type kernel parameters.
We use a simple filtering policy that the runtime taint tracking only operates on
instructions whose operands can be reached from potential global memory taint sources
through dependencies and can reach potential global memory taint sinks.  
We present an iterative two-pass taint reachability analysis to implement such
instruction filtering which significantly reduces runtime taint tracking costs.

Our taint tracking system also exploits the
heterogeneous memory architecture on GPUs. A GPU
has different types of memory, including either physically partitioned or
logically partitioned memory storage. For instance, local memory is private to
every thread, shared memory is a software cache visible to a group of
threads, and global memory is visible to all threads. Our taint system handles
different types of memory storage separately and optimizes the tracking for
different types of memory storage. Specifically, we allocate a portion of the register file
to store part of the taint map, since GPU contains a much larger register file
than CPU---e.g., every streaming multi-processor (SM) has 64K registers
on most NVIDIA GPUs. Not all registers are needed \cite{Gebhart+:MICRO2012,Li+:CGO2015}
nor the maximum occupancy is necessary \cite{hayes-ics-2014} for best
performance. Using fast access registers to maintain the taint map of frequently accessed
data will improve the dynamic tainting performance.

GPU taint tracking enables data protection
that clears sensitive (tainted) data objects at the end of their life
range as well as detects leak of the sensitive data in the midst of program execution.
We recognize that data in different GPU memory storage may have different life ranges.
For instance, registers and local memory are thread private and can be cleared once a thread
finishes its execution; shared memory is only used by a thread block and
sensitive data in shared memory can be cleared by that thread block once it
releases the SM\@. Global memory may be accessed at any time of a program run so we
cannot clear it at the end of every kernel execution.
However, we can detect when and where the sensitive information (in global memory) is sent out by
instrumenting memory communication APIs since all communication between GPU, CPU and other
network devices require explicit memory API calls. By checking if the
sensitive information falls within the region of memory that is transferred, we can
identify GPU malware (like Keylogger~\cite{ladakis-eurosec-2013} and
Jellyfish~\cite{jellyfish-web}) that uses GPU to snoop CPU activities
while storing these activities in GPU memory.
Such GPU-resident malware would escape detection by a CPU-only taint tracking mechanism.

%Overall, our tool provides opportunities for 
%per-application data clearing, protection and monitoring ---every (group of) threads can
%protect, clear and encrypt their own sensitive data at different function calls
%or other granularity at program level. %not clear in current systems

%traditional taint tracking work on CPUs cannot be directly used on CPUs. In
%CPUs, dynamic instrumentation is used frequently, for instance, PIN, however it
%is not feasible to do so on GPUs, as there are thousands of threads running at
%the same time, tracking each thread will be prohibitive. Static instrumentation
%and dynamic tracking is used in CPUs, however, there is no study on the impact,
%the overhead, the trade-off of static binary instrumentation at GPUs. Naive
%static instrumentation instruction by instruction has a lot of overhead. In this
%paper, we present a novel and low overhead taint tracking approach for GPUs,
%which is based on two level optimizations, and tracking of all different types
%of memories on GPU. 

% Our contributions are summarized as follows:
% \begin{itemize}
% \item We present the first design and implementation of a GPU taint tracking system. 
% Our system utilizes static binary-level instrumentation to enable runtime taint tracking.
% %Our method is faster and more feasible than the traditional dynamic
% %instrumentation methods on CPUs. 
% \item We present two optimizations that exploit the characteristics of GPU programs
% and architecture to reduce monitored instructions for taint tracking and to 
% accelerate the taint map maintenance respectively.  Together these techniques
% reduce tracking overhead by 5 to 20 times.
% \item Our taint analysis framework serves as a critical foundation for data
% protection and security attack countermeasures on GPUs. We describe possible case
% uses to help protect sensitive data and counter GPU-resident malware.
% %We not only track sensitive information flow but can also clear data before its end of live range, at program levels.
% \end{itemize}

%The rest of this paper is organized as follows. 
%Section~\ref{sec:back} describes background information of relevant GPU memory
%architecture and information leaking vulnerabilities.
%Section~\ref{sec:tainting} presents our optimization techniques to improve
%GPU tainting performance.
%Section~\ref{sec:cases} describes two case studies of tainting-enabled data
%protection.
%Section~\ref{sec:eval} experimentally evaluates the performance and
%effectiveness of proposed techniques.
%Section~\ref{sec:related} discusses related work
%and Section~\ref{sec:conclude} concludes this paper.

%In recent years, General-Purpose Graphics Processing Units (GPGPUs) have become popular for a range of applications, including machine learning, image processing, and more. However, due to their relative infancy, GPGPUs have undergone much less study than CPUs. One important technique which is well-studied on CPUs is dynamic taint analysis~\cite{TODO}. This is a form of data-flow analysis wherein some set of sensitive input data is marked as `tainted', and this taint is tracked during runtime as it spreads to different locations in memory via move operations, arithmetic operations, etc, tracing dependencies. Taint analysis is useful for detection of certain types of malicious and erroneous behavior, which remains relevant on the GPU, yet there are no prior works which consider its use for such architectures.

%Any programs which rely on dynamic taint analysis and take advantage of GPU acceleration are incomplete if they perform only traditional, CPU-level taint analysis. For example, in web applications, taint analysis has been used as a means of detecting SQL injection, marking untrusted user inputs as tainted~\cite{xu2006taint}. In~\cite{bakkum2010accelerating}, NVIDIA GPUs are used to obtain significant speedup for SQL queries, but the security implications are not considered. If any existing means of taint tracking is applied in such a case, then taint propagation taking place during GPU execution will be missed, leading to potential vulnerability.

%Current GPUs also exhibit less memory protection than is present on other architectures, which may lead to a greater need for some form of taint analysis. \cite{pietro-tecs-2016} shows that NVIDIA GPUs leave data behind in DRAM, cache, and registers, allowing later programs to simply allocate memory and read these values. \cite{pietro-tecs-2016} demonstrates theft of encryption keys on the GPU, and \cite{lee2014stealing} demonstrates theft of screen rendering data left behind by a web browser. In such cases, taint tracking can be applied to either the leaking program, in order to erase sensitive data as appropriate, or to untrusted programs, marking uninitialized and out-of-bounds data as taint sources in order to detect malicious or erroneous behavior.

%Taint tracking on the GPU is complicated by several factors. Memory is logically and physically split into a larger number of partitions than on CPU, each of which have different accessibility and different patterns of allocation. Additionally, the drivers and hardware for the architecture targeted in this paper (NVIDIA) are closed-source, preventing any straightforward system-wide monitoring of GPU operations.

%In this paper, we present the first work on GPU taint analysis. We implemented a framework which uses binary-level instrumentation to perform dynamic taint tracking across the various types of memory used by the GPU. We describe optimizations used to minimize the overhead of taint analysis, and we evaluate the effectiveness of these optimizations on a modern NVIDIA GPU over multiple benchmarks, including GPU kernel functions in the Caffe Deep Learning Framework~\cite{TODO}. Finally, we discuss specific GPU applications where taint tracking can be used to solve existing problems which lack any other solution. %TODO probably format this into bullet points as a list of contributions.
