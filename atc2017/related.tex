\label{sec:related}

Dynamic taint analysis~\cite{chow-uss-2004,newsome-ndss-2005,ho-eurosys-2006,qin-micro-2006,xu-uss-2006,clause-dytan-2007,Enck+:TOCS2014,zhu-osr-2011}
tracks data (and sometimes control) dependencies of information as a program or system
runs.  Its purpose is to identify the influence of taint sources on data storage locations (memory,
registers, etc.) during execution.  Taint tracking is useful for understanding data flows
in complex systems, detecting security attacks, protecting sensitive data, and analyzing
software bugs.  Its implementation usually involves static code transformation,
dynamic instrumentation, or instruction
emulation using virtual machines to extend the program to maintain tainting metadata.
While existing dynamic tainting systems track CPU execution, this paper presents
the first design and implementation of a GPU taint tracking system.

A large body of previous work presented techniques to improve the performance of
CPU taint tracking.
LIFT~\cite{qin-micro-2006} checks whether unsafe data are involved before a code
region is executed, and if not, no taint tracking code is executed for that code
region to reduce overhead.  Minemu~\cite{bosman+:2011} proposes a novel memory
layout to reduce the number of taint tracking instructions. It also uses SSE
registers for taint tracking to reduce performance overhead.  
TaintEraser~\cite{zhu-osr-2011} makes use of function summary to reduce the
performance overhead of taint tracking. It summarizes taint propagation at the
function level so that instruction level taint tracking is reduced.
TaintDroid~\cite{Enck+:TOCS2014} is a taint analysis tool proposed for Android
systems. By leveraging Android’s virtualized execution environment and
coarse-grained taint propagation tracking, it can achieve nearly real time
analysis with low performance overhead.
Jee et al.~\cite{Jee+:NDSS2012} proposed to separate taint analysis code from the
original program, and dynamic and static analysis was applied on the taint
analysis code to optimize its performance.
In this paper, we present new performance optimizations by exploiting unique GPU 
characteristics.
%---a large portion of instructions on GPU runtime parameters
%and ``constant memory'' can be safely eliminated;
%large GPU register file allows fast retrieval of hot region of taint map.  

Security vulnerabilities on GPUs have been recognized recently.
Dunn et al.~\cite{dunn-osdi-2012} showed that sensitive data can be leaked into
graphics device driver buffers.  They proposed encryption to protect data in
transit over the device driver but their approach does not protect data in GPU memory.  
Lee et al.~\cite{lee-sp-2014} uncovered several vulnerabilities of leaking 
sensitive data in GPU memory---leaking global memory data after a program context
finishes and releases memory without clearing; leaking local memory data across
kernel switches on a CU\@.  They did not present any solution to address these
vulnerabilities.  More recently, Pietro et al.~\cite{pietro-tecs-2016} proposed
memory zeroing to prevent information leaking in GPU\@.  However, memory zeroing alone
provides limited protection---it cannot track information flow in memory;
nor can it counter GPU malware such as Keylogger~\cite{ladakis-eurosec-2013} and
Jellyfish~\cite{jellyfish-web}.  Furthermore, GPU tainting is complementary to
memory zeroing---tainting identifies
a subset of sensitive memory for zeroing to reduce the costs.

GPU information flow analysis has been performed in the past.
Leung et al.~\cite{leung-pldi-2012} and Li et al.~\cite{li-sc-2014} employed
static taint analysis to reduce the overhead of GPU program analysis and verification.
Static analysis requires memory aliasing analysis of memory accesses that
are inherently imprecise.  While they are suitable for testing and debugging purposes~\cite{leung-pldi-2012,li-sc-2014},
security data flow analysis in this paper requires more precise dynamic tracking.
Farooqui et al.~\cite{Farooqui+:GPGPU2014} proposed static dependency analysis between
thread index and control conditions to identify possible thread divergence in GPU
executions (the result of which helps determine whether symbolic execution can be
performed on given GPU basic blocks).  Their static dependency analysis is narrowly
targeted and it is unclear whether it applies to general taint tracking.

%A number of recent studies have utilized parallelism or speculative execution to
%facilitate or accelerate program analysis.
%%\notered{TightLip~\cite{yumerefendi-nsdi-2007}.}
%ShadowReplica~\cite{Jee+:CCS2013} employs a secondary
%thread to perform analysis to avoid slowdown for the primary thread, and they
%communicate and synchronize with each other through a memory buffer.
%Shadow Profiling~\cite{moseley-cgo-2007} parallelizes application profiling by running them in
%shadow processes instead of the production run.
%TaintPipe~\cite{Ming+:Security15} has multiple taint analysis threads to pipeline
%tainting work and achieve better performance. It also proposes symbolic taint
%analysis to avoid stall due to unready taint input.  
%SuperPin~\cite{Wallace+:CGO2007} parallelizes dynamic instrumentation tool Pin~\cite{lu-pldi-20k05-pin}.
%Instrumented code are executed in parallel with the original code to hide each
%other’s latency.
%%Aftersight~\cite{Chow+:ATC2008} records nondeterministic events during execution
%%in a virtual machine system and replays the execution separately to perform
%%analysis so that the performance of the original execution is not affected.
%%Traditionally, the main program is not allowed to proceed before security check completes.
%Speck~\cite{Nightingale+:ASPLOS2008} uses replays to run security checks in parallel
%so that it does not slow down the primary execution.
%Parallelism-enabled optimization is particularly attractive on GPUs that support
%a high degree of parallelism.  This is a promising future direction for fast GPU taint tracking.
