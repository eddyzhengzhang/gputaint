\label{sec:tainting}

A typical information flow tracking system on CPUs monitors instructions and 
operands to maintain proper taint propagations.
For example, in a binary operation $v$ $=$ $binop$ $v_1$,
$v_2$, assuming $T(v_1)$, $T(v_2)$, and $T(v)$ represent the taint status for
operands $v$, $v_1$, and $v_2$ respectively: true means tainted and false means
untainted. The taint tracking rule for this instruction is $T(v)$ $=$ $T(v_1)$ $||$
$T(v_2)$.  Taint statuses for all data storage locations (program memory, registers, 
conditional flags, etc.) are maintained in a taint map in memory.
A baseline GPU taint tracking system would operate in a similar way.

Dynamic taint tracking~\cite{chow-uss-2004,newsome-ndss-2005,ho-eurosys-2006,qin-micro-2006,xu-uss-2006,clause-dytan-2007,Enck+:TOCS2014,zhu-osr-2011}
is known to incur high runtime costs.
Fortunately, GPU executions exhibit some unique characteristics that enable optimization.
We present an optimization that recognizes and identifies the large portion of GPU
instructions that cannot be involved in taint propagation from sources to sinks.
Furthermore, given the large register file on GPU and frequent register accesses, we maintain
register taint map in registers to accelerate their taint tracking.
These optimizations are performed through binary-level static analysis.

%\begin{figure}
%\caption{An example of not all operands need to be tracked.}
%\label{fig:gpumot}
%\end{figure}

\vspace{-0.1in}
\subsection{Taint Reachability}
\vspace{-0.1in}
On GPUs, we discover that programs frequently operate on a set of critical runtime
un-taintable values, and that not all operands need to be
tracked. We exploit this fact and only track the operands that potentially carry
taints or may have an impact on the state transition of the un-taintable objects. 
In the earlier example, if $v_1$ does not carry any taint, the taint
maintenance only needs to track $v_2$ and $v$ such that $T(v) = T(v_2)$. If
neither $v_1$ or $v_2$ can be tainted, or if $v$ does not propagate to memory,
no taint maintenance is necessary for the variables $v$, $v_1$, and $v_2$.

%We find out we can significantly reduce the number of taint tracking
%instructions and improve dynamic information flow tracking efficiency. 

A frequently used GPU runtime un-taintable is the logical thread index. A thread
index is used to help identify the task that is assigned to every thread. It is
a built-in variable, and does not come from global memory that is managed by a
programmer, and thus the instruction operand as a thread index or an expression
of thread index can never be tainted.  Similarly,
other built-in thread identification variables, including thread block id and
dimension configuration, are also un-taintable.

Another frequently used GPU runtime un-taintable value are the non-scalar pointer-type
kernel parameters. A GPU kernel function does not allow call-by-reference. To
reference a memory data object that can be modified at runtime, it can
only use pointers. Moreover, these kernel parameters are kept in a memory region
named as ``constant memory" in GPUs and are read-only in kernel execution. The memory region pointed to by the kernel
parameter must be tracked, but the pointer or the address expression
computed using the pointer and thread index (or part of the expression)
does not need to be tracked. Other examples include compile-time 
un-taintable values, such as loop induction variables and stack framework pointers, programmer-specified
constants, and combinations of GPU-specific runtime constants with these
constants. We analyze and categorize these un-taintable values in 
Section~\ref{sec:eval} and Table~\ref{tab:filterdetail}.

%We exploit the fact that some instruction operands only depend on untaitable
%sources, to eliminate the instrumentation instructions so that we only focus on the instruction operands that can potentially be reached from taintable sources, and significantly
%reduce the amount of dynamic taint tracking cost.

To avoid tracking un-taintable values in GPU programs, we
take the following approaches.  
\begin{enumerate}
\item We classify an instruction operand into two types: taintable and un-taintable. The \emph{taintable} state
indicates that the operand might be tainted at runtime---whether it will be really
tainted depends on the exact dynamic analysis done by tracking instructions. The
\emph{un-taintable} state indicates that the operand cannot be
tainted at runtime. Any operand that cannot be reached from the taintable
source is un-taintable. 
The taintable sources are program inputs given by the users and reside in the global
memory on GPUs.  Examples include face recognition photos, a plain-text message,
and encryption key.

%In most cases, we do not need to insert any tracking instruction for
%un-taintable operand at static-time (since by default everything is un-tainted
%except the taint sources).
 %as long as this instruction operand is not at
%the point where the (un-)taintable state might change (which is described next). 

\item A variable can be overwritten with taintable or
un-taintable values at different program execution points.
We check for potential state transition of a variable: from
un-taintable to taintable, or from taintable to un-taintable.  The latter arises
in a situation called \emph{taint removal}---e.g., assigning a constant to a register
who might be in a tainted state before the assignment but must now
transition to the un-tainted state.

\item We statically check the memory reachability: whether an operand might reach
memory (potential taint sinks). Even if an operand is taintable, as long as it
does not flow into memory, it will not affect any taint sink. We do not need to add
tracking instruction for this type of operands.  Common examples include
loop trip counters, predicate registers, and stack frame pointers.
\end{enumerate}

\begin{figure}[htb]
\centering
\includegraphics[width=\columnwidth]{fig/track_untaintable}
%\caption{An Example of Tracking Un-taintable Data}
\label{fig:taintstate}
\end{figure}

\vspace{-0.2in}
We show an example in the code snippet above.  The code describes a loop.
Register R0 is overwritten with different types of values. Initially R0
is written with an un-taintable value (lines~2-3). Later in the $if$ statement, it is written by a
taintable value [R1]; note that here the [R1] notation indicates a memory
operation and the address of the memory location is R1.  We need a
tracking instruction within the $if$ statement since [R1] comes from global
memory and every operand from memory needs to be tracked. 
We do not need a tracking instruction for line~2 since $0x1234$ is a
constant and the assignment target R0 at line~2 cannot reach memory.
However, we do need a tracking instruction for line~3 since the assignment target
may reach memory and taint removal applies here (R0 may be tainted from
an earlier iteration of the loop and if so, taint must be removed here).

%The reason is that when the loop is executed, $R0$
%might be overwritten with an un-taintable value $0x0$ (line 5) right after $R0$ is
%marked as tainted in the previous iteration in which the $if$ branch is
%taken. The tracking instruction is $T(R0) = false$, and if we do not add this
%tracking instruction, in all the following iterations, R0's state will be
%marked as tainted even though it might not be (if in the future iterations, the
%``if" branch is not taken).  Therefore, we need to add a tracking instruction at
%line 3 even if the operand $R0$ is un-taintable. 

%The implication for tracking tainting state transition is that not
%only do taintable operands need to be tracked, but also non-taintable operands need
%to be tracked when the taint state of a register is changed.

\vspace{-0.1in}
\subsection{Iterative Two-pass Taint Analysis}
\vspace{-0.1in}
To mark the taintability and reachability attributes for every operand and to
detect potential taint state transition, we perform an iterative dataflow
analysis. 
%If an operand is either
%taintable or incurs a potential state change, it will be tracked, otherwise it
%will be not. 

There are two passes in our iterative dataflow analysis component. The forward
pass marks the taintable operands and the un-taintable operands only at the
program points where a potential taint state transition occurs. The backward
pass marks an operand that potentially reaches memory (taint sinks). In the end,
when adding code to track the original program, we only track the operands
that are marked in both forward and backward passes. 
%%the intermediate
%%variables generated at compile time, . 

%Our static instruction filtering attempts to identify all instruction operands that may
%affect information flow from potential taint sources (memory) to potential taint sinks
%(memory).  Dynamic taint tracking work at runtime would only have to operate on these
%instructions.  We statically identify two kinds of instruction operands:

\begin{figure}[htb]
\centering
\includegraphics[width=\columnwidth]{fig/taintSystemOverview.pdf}
\caption{Overview of our taint tracking system.}
\label{fig:taintreach}
\end{figure}
Figure~\ref{fig:taintreach} provides an overview of our taint tracking system.
First, we analyze the binary code to obtain the control flow graph and a list of basic
blocks. A basic block is the maximum length single-entrance and single-exit code
segment. We also mark the operands that are known to be un-taintable before the
program starts. They include built-in thread identification variables,
non-scalar pointer type kernel parameters, and other programmer-specified constants.    

% move here for better co-location with text
\begin{figure*}[htp]
\centering
\includegraphics[width=1.0\textwidth]{fig/forward.pdf}
\vspace*{-0.25in}
\caption{Forward taint reachability analysis.}
\vspace*{-0.1in}
\label{fig:filteralg}
\end{figure*}

We perform the backward pass first to analyze each operand and set its memory
reachability attribute. We name it the \emph{mightSpread} attribute, indicating
whether there exists an execution path through which the value of this operand might
spread into memory. 

We then perform the forward pass to mark all operands as taintable
or un-taintable, and for every un-taintable operand, we also analyze if its last
immediate state is taintable in one of the potential execution paths.
If an operand is taintable or 
its last immediate state is taintable, we set the
\emph{taintTrack} attribute to be true.  The taintTrack attribute indicates
that the operand may be tainted at runtime.  For an indirect memory operand,
we also need an attribute on the taintability of the addressing register.  We call
this \emph{addrTrack} attribute.

Finally, in the \emph{Tracking Filter} component, we scan all instructions and
review the taintability and reachability attributes each operand. For the 
destination operand, if its taintTrack and mightSpread attributes
are both true, we add tracking code for this destination operand, otherwise we
don't. Similarly, for source operands, if both of its taintTrack and
mightSpread attributes are true, we add tracking code for the source
operand before the tracking code for the destination operand.
For an indirect memory source operand, if its addrTrack and mightSpread attributes
are both true, we add taint tracking code for the source
operand addressing register.

We describe the detailed algorithms for forward and backward passes below.

\vspace{-0.1in}
\paragraph{Forward Taint Reachability Analysis}
The input is a control flow graph and a set of basic blocks for the GPU program. The output is the taintTrack property value for every operand in every
instruction. We show the forward pass algorithm in Figure~\ref{fig:filteralg}(a).

We adopt the fixed-point computation algorithm that is used in standard dataflow
analysis (DFA) framework. Function \emph{forward\_pass} in
Figure~\ref{fig:filteralg}\emph{(a)} scans the basic blocks one by one, sets
the taintTrack attribute for every operand, and updates the
\emph{taintTrackBeg} attribute for every basic block. Our forward analysis pass checks if one basic
block's taintability updates affect another basic block's taintability results,
and if so, adds the affected basic block to the worklist. Initially, all basic
blocks are added to the list. The analysis pass finishes only when all basic
block's taintability results do not change. 

A DFA problem is formulated using (a set of) dataflow equation(s). We describe the dataflow
equation as follows. The taintTrackBeg
attribute describes the taint tracking state of every register at the beginning
of a basic block, which is a bit array. Every bit in the bit array corresponds
to one physical register. If a register's taintTrack attribute is true at
the beginning of the basic block of interest, this bit is set
to 1, otherwise 0. Assume a basic block $P$ and it has $n$ predecessor basic
blocks $Q_i$, $i=1...n$, the dataflow relation is 

{
\centering
P.taintTrackBeg  = $\cup$ forward\_prep($Q_i$,$Q_i$.taintTrackBeg). \par
}
\vspace{2pt}

The \emph{forward\_prep} function in Figure~\ref{fig:filteralg}(b) updates the
taintability state for all instructions in a basic block based on the
taintability state at the beginning of the basic block. It scans the first
instruction to the last instruction. 

Given an instruction, the \emph{forward\_prep} function checks its source
operands first (lines 3$-$6 in Figure~\ref{fig:filteralg}(b)). If a
source operand is register and the taintTrack attribute is true, this source
operand needs to be tracked. If a source operand is of memory type, it has to be
tracked. Note that if the address register of an indirect memory operand is
taintable, we need to track the register as well---setting addrTrack attribute
at line 6 in Figure~\ref{fig:filteralg}(b).

Next, the \emph{forward\_prep} function checks every destination operand. If
any source operand needs to be tracked based on the above analysis, destination
operand needs to be tracked as well. In the meantime, we update the register tracking state
for the corresponding destination operand (line 10 in Figure \ref{fig:filteralg}(b)).
If the destination operand is of memory type, it needs to be tracked. If the destination operand is
un-taintable (lines 13-15 in Figure \ref{fig:filteralg}), and its prior tracking
state is taintable, and the destination operand might spread to memory, the
destination operand needs to be tracked as well. Further we update the register
tracking state for the corresponding destination operand.

\begin{figure}[htb]
\centering
\includegraphics[width=1.0\columnwidth]{fig/forwardpass_expl}
%\caption{forward preprocessing example}
%\label{fig:forwardexample}
\end{figure}

% move here for better co-location with text
\begin{figure*}[htp]
\centering
\includegraphics[width=1.0\textwidth]{fig/backward.pdf}
\vspace*{-0.25in}
\caption{Backward memory reachability analysis.}
\vspace*{-0.1in}
\label{fig:filteralg2}
\end{figure*}

We use the above example to illustrate the {\bf forward\_prep} step for updating the register
tracking state. Let the initial regTaintState be
[0, 1, 0, 0], meaning that only register R1 is found to be taintable on 
entry to this basic block. Since the first instruction has R1 as a source and R0 as a 
destination, we set the operand's taintTrack flag and regTaintState[0] to true.

Since the second instruction writes an immediate value to R1, but since regTaintState[1] was 
previously true, we have to set the operand's taintTrack flag to true, if its result can spread to memory. 
This instruction potentially changes the taint value of R1 at runtime from true to false, 
so if it can reach memory, then we need to instrument it, or else we will suffer from over-tainting 
as a result of incorrectly treating the data as still being tainted. We flip 
regTaintState[1] to false since at compile-time and at the second instruction,
register R1 is untaintable.

The next instruction loads from memory into R2, so we set the operands'
taintTrack flags 
and regTaintState[2] to true, because memory is a possible taint source. The final 
instruction before the branch carries potential taint from R0 to R3; since regTaintState[0] 
is true, regTaintState[3] is set to true along with the operand's taintTrack flag.

\vspace{-0.1in}
\paragraph{Backward Memory Reachability Analysis}
Similar to the forward pass, the
backward pass uses the program as input. The output is the memory reachability
property of every operand. The backward reachability analysis also uses a
dataflow analysis framework, which solves the \emph{mightSpreadBeg} bit
array for every individual basic block, representing the memory reachability state of the registers at the beginning of basic
block. In this bit array, each bit corresponds to one physical register. 
A value of 1 for the bit at index $n$ of basic block $b$ means that
the value of register $Rn$ at the beginning of basic block $b$ might reach memory. 

 The relationship between one basic block $P$ and its
successor basic blocks $Q_i,i=1..m$, where $m$ is the total number of immediate
successor basic blocks, is described using the following equation:

{\centering
$P$.mightSpreadBeg = backward\_prep($\cup$ $Q_i$.mightSpreadBeg).
\par
} 
\vspace{2pt}

The initial mightSpreadBeg bit array is set to 0 for every basic block. Our backward
pass keeps updating the mightSpreadBeg bit arrays until they do not change
any further (Figure \ref{fig:filteralg2}(a)).
In the meantime, the attribute mightSpread is updated for every operand,
as described in Figure~\ref{fig:filteralg2}(b). 

The \emph{backward\_prep} function calculates mightSpreadBeg for every individual basic
block. In Figure~\ref{fig:filteralg2}(b), we scan the instructions in
reverse order in a basic block. First, we check the destination operand, if it
is register type and the register's memory reachability state is true, the
destination operand's mightSpread attribute is set to true. In the meantime, we update the
register's memory reachability state for the destination register to false since the
value to spread into memory is defined at this point and for
any instruction that happens before this instruction, they don't see the same
value as defined here. If it is memory type, the mightSpread attribute is
set to true and the address register's reachability state is set to
true (line 7 in Figure~\ref{fig:filteralg2}(b)).  
Next, we check the source operands. If any destination operand can spread into
memory, then all source operands' mightSpread property is set to true (line 10).
Correspondingly, we will set the register reachability state to true (line 11).

\begin{figure}[H]
\centering
\includegraphics[width=0.85\columnwidth]{fig/backwardpass_example}
%\caption{backward preprocessing example}
\vspace*{-0.1in}
%\label{fig:backwardexample}
\end{figure}

We use the above example to illustrate the process for updating the mightSpreadBeg
bit arrays in the backward pass.
The backward pass is mechanically similar to the forward pass, aside from the 
direction in which instructions are processed. In this example, we assume that 
registers R1 and R3 have been determined to spread to memory in later blocks, hence 
the initial regSpreadState value of [0, 1, 0, 1]. We skip over the branch instruction
since it has no operands except for a jump offset. 

The last instruction has data 
flow into R3 from R0, and regSpreadState[3] is true, so we mark the R0 operand's
mightSpread flag as true and 
set regSpreadState[0] to true. We also flip regSpreadState[3] to false since this 
instruction is overwriting R3. 

The instruction before the last stores register R2 to memory, 
so we simply mark the R2 operand's mightSpread flag as true and set regSpreadState[2] to true. 

The third
instruction counting from the last puts an immediate value into R1, so we set regSpreadState[1] to false.
Finally, the fourth instruction counting from the last has data flow into R0 from R1 and R2, and regSpreadState[0] 
is true, so we mark both source operands' mightSpread as true, set regSpreadState[1] and regSpreadState[2] 
to true, and set regSpreadState[0] to false since R0 has been overwritten.

\vspace{-0.1in}
\subsection{Register Taint Map in Registers} 
\vspace{-0.1in}
A GPU contains a much larger register file than CPU does---e.g., every streaming
multi-processor has 64K registers on most NVIDIA GPUs. 
Registers are naturally accessed frequently and maintaining their taint statuses
require frequent reads and writes from/to their taint map locations.
At the same time, the large GPU register file presents the opportunity to
maintain a portion of the taint map in registers.
These facts motivate us to place the register taint map in registers.

We use multiple 32-bit general
purpose registers to store the taint map, in which one bit corresponds to one
register that is tracked. Using register-stored taint map increases the number
of registers used per-thread, and might decrease occupancy, determined
as the number of active threads running at the same time. Fortunately in many
GPU programs, not all the register file is needed~\cite{Gebhart+:MICRO2012,Li+:CGO2015} 
nor the maximum occupancy is necessary~\cite{hayes-ics-2014} for the best program
performance.  Therefore the overall taint tracking cost is significantly reduced
by our use of register-stored taint map, as demonstrated later in evaluation.
