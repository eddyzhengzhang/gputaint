\documentclass[10pt,twocolumn]{article}

\usepackage{times}
\usepackage{cite}
\usepackage{epsfig}
%\usepackage{subfigure}
\usepackage{subcaption}
\usepackage{xspace}
\usepackage[shortcuts]{extdash}
\usepackage{caption}
\usepackage{url}
\usepackage{color}

\def\baselinestretch{1.00}

% suppress page numbers.
%\pagestyle{empty}

%set dimensions of columns, gap between columns, and space between paragraphs
\setlength{\textheight}{9.0in}
\setlength{\textwidth}{6.5in}
\setlength{\columnsep}{0.25in}
%\setlength{\footskip}{0.0in}
\setlength{\topmargin}{0.0in}
\setlength{\headheight}{0.0in}
\setlength{\headsep}{0.0in}
\setlength{\oddsidemargin}{0.0in}
\setlength{\evensidemargin}{0.0in}
%%\setlength{\parindent}{0pc}
%%\setlength{\parskip}{\baselineskip}

% started out with art10.sty and modified params to conform to IEEE format
% further mods to conform to Usenix standard

\makeatletter

%as Latex considers descenders in its calculation of interline spacing,
%to get 12 point spacing for normalsize text, must set it to 10 points
\def\@normalsize{\@setsize\normalsize{12pt}\xpt\@xpt
\abovedisplayskip 10pt plus2pt minus5pt\belowdisplayskip \abovedisplayskip
\abovedisplayshortskip \z@ plus3pt\belowdisplayshortskip 6pt plus3pt
minus3pt\let\@listi\@listI}

%need a 12 pt font size for subsection and abstract headings
\def\subsize{\@setsize\subsize{12pt}\xipt\@xipt}

%make section titles bold and 12 point, 2 blank lines before, 1 after
%\def\section{\@startsection {section}{1}{\z@}{24pt plus 2pt minus 2pt}
%{12pt plus 2pt minus 2pt}{\large\bf}}
\def\section{\@startsection {section}{1}{\z@}{16pt}{9pt}{\large\bf}}

%make subsection titles bold and 11 point, 1 blank line before, 1 after
%\def\subsection{\@startsection {subsection}{2}{\z@}{12pt plus 2pt minus 2pt}
%{12pt plus 2pt minus 2pt}{\subsize\bf}}
\def\subsection{\@startsection {subsection}{2}{\z@}{9pt}{9pt}{\subsize\bf}}

\makeatother

\newcommand{\notered}[1]{\textcolor{red}{[{\bf #1}]}}

\def\code#1{\texttt{\small #1}}

\begin{document}

% Seem to be necessary for letter-size (instead of A4-size) PDF
\setlength{\pdfpageheight}{11in}
\setlength{\pdfpagewidth}{8.5in}

\title{\Large\bf GPU Tainting}

\author{}
\date{}
\maketitle

\begin{abstract}
\end{abstract}

\section{Motivations}

Dynamic taint analysis~\cite{chow-uss-2004,newsome-ndss-2005,ho-eurosys-2006,qin-micro-2006,xu-uss-2006,clause-dytan-2007,enck-taintdroid-2010,zhu-osr-2011}
tracks the data (and sometimes control) dependencies of information as a program or system
runs.  Its purpose is to identify the influence of taint sources on data storage locations (memory,
cache, registers, etc.\@) during execution.  It is widely used for understanding data flows
in complex systems, detecting security attacks, protecting sensitive data,
and analyzing software bugs.  Existing realizations of dynamic taint analysis only apply
to CPU executions.  In this paper, we argue that dynamic tainting is also useful for data
flow analysis and information security on computational accelerators such as GPUs.  Below 
we highlight several usage scenarios for GPU tainting.

Lee et al.~\cite{lee-sp-2014} and Pietro et al.~\cite{pietro-tecs-2016} 
have recently demonstrated that information leaking from one program to another may occur in
GPU local memory between GPU kernel executions, and in GPU global memory between program runs.
Taint tracking may help a program to understand the propagation of certain sensitive
information and clear all taints before relevant points of vulnerability (e.g., clearing local
memory taints at the end of each kernel and clearing global memory taints at the end of 
program run).
We will make a prototype implementation of this use case and perform experimental evaluation.
\notered{We need to argue that this isn't just a performance argument---there isn't an obvious
way to identify all data in memory written by an application even if we want to clean data
indiscriminately.}

Sometimes it is desirable to protect sensitive data before the end of its lifetime.  Much like
CleanOS~\cite{tang-osdi-2012} in smartphone data protection, tainting can help protect live
but inactive sensitive data through encryption.  The objective in this case is to reduce
(but not to eliminate) the exposure of sensitive data.
This protection is useful on mobile platforms where the device may physically fall into the possession
of a malicious party (through loss or theft) and its entire
memory may be inspected (e.g., through cold boot RAM imaging attacks~\cite{halderman-uss-2008}).
This protection is also useful in cases when concurrently running GPU programs may see each other's
global memory.  \notered{Could this only happen in the ``register spilling'' case described
in~\cite{pietro-tecs-2016}?  I am not sure this can be argued as a general threat.}

Furthermore, tainting can help monitor the use and influence of uninitialized data in GPU execution
and capture taints of uninitialized data in GPU outputs.  Capturing such taints can improve
application correctness as outputs that depend on uninitialized data may be non-deterministic.
Furthermore, if such taint tracking is securely applied to untrusted programs,
it can capture possible attempts of stealing information of previous kernel and program
runs by other users. 

Finally, in tainting applications that utilize both CPU and GPU, complete taint tracking during
both CPU and GPU executions is essential for full system security.  For instance, a web site
may rely on taint tracking to prevent untrusted user inputs from forming malicious 
database queries (e.g., through SQL injections) or invoking dangerous system calls
(e.g., through buffer overflow attacks).  A dynamic taint tracking system that only monitors
data dependences during CPU execution may miss the influence propagation of untrusted
inputs through GPU execution.

\notered{The above are possible motivating scenarios.  It is unlikely that we will be able to
produce full implementation and demonstration for all these scenarios.  We may have to focus
on the basic GPU tainting implementation and then focus on one or two utilizations.}

\bibliographystyle{abbrv}
\bibliography{paper}

\end{document}
